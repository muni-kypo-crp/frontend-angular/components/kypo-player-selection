import { PlayerData } from "./player-data";

export class PlayerSelection extends PlayerData{
    isSelected: boolean = true;
    isHovered: boolean = false;
    alertMessage: string = '';

    hasAlert(): boolean {
        return this.alertMessage.length > 0;
    } 
}