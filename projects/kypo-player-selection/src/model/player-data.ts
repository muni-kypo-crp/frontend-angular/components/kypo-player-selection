import { Player } from "./player";

export class PlayerData {
    id!: number;
    player!: Player;
    alertMessage!: string;

    constructor(id: number, player: Player, alertMessage: string) {
        this.id = id;
        this.player = player;
        this.alertMessage = alertMessage;
    }

    hasAlert(): boolean {
        return this.alertMessage.length > 0;
    } 
}