import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { KypoPlayerSelectionmaterialModule } from './kypo-player-selection-material.module';
import { KypoPlayerSelectionComponent } from './kypo-player-selection.component';
import { PlayerTabComponent } from './player-tab/player-tab.component';

@NgModule({
  declarations: [KypoPlayerSelectionComponent, PlayerTabComponent],
  imports: [CommonModule, KypoPlayerSelectionmaterialModule],
  exports: [KypoPlayerSelectionComponent],
})
export class KypoPlayerSelectionModule {}
