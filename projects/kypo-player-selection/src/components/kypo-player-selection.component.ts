import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { View } from '../model/enums/view';
import { PlayerData } from '../model/player-data';
import { PlayerSelection } from '../model/player-selection';

@Component({
  selector: 'kypo-player-selection',
  templateUrl: './kypo-player-selection.component.html',
  styleUrls: ['./kypo-player-selection.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KypoPlayerSelectionComponent implements OnInit {
  @Input() playerData!: PlayerData[];
  @Input() view: View = View.BOTH;

  @Output() playerSelectionChange: EventEmitter<number[]> = new EventEmitter();
  @Output() hoveredPlayerChange: EventEmitter<number> = new EventEmitter();

  playerSelections!: PlayerSelection[];

  constructor() {}

  ngOnInit(): void {
    this.playerSelections = this.playerData.map(
      (playerData: PlayerData) => new PlayerSelection(playerData.id, playerData.player, playerData.alertMessage)
    );
  }

  onPlayerSelectionChange(playerSelection: PlayerSelection): void {
    this.playerSelections.find((ps) => ps.id === playerSelection.id)!.isSelected = playerSelection.isSelected;
    this.emitSelected();
  }

  private emitSelected(): void {
    this.playerSelectionChange.emit(
      this.playerSelections
        .filter((playerSelection) => playerSelection.isSelected)
        .map((playerSelection) => playerSelection.id)
    );
  }

  onPlayerTabHover(playerSelection: PlayerSelection) {
    this.hoveredPlayerChange.emit(playerSelection?.id);
  }

  getSelectedPlayersCount(): number {
    return this.playerSelections.filter((playerSelection) => playerSelection.isSelected)?.length;
  }

  showAllPlayers(): void {
    this.playerSelections.forEach((playerSelection) => (playerSelection.isSelected = true));
  }

  hideAllPlayers(): void {
    this.playerSelections.forEach((playerSelection) => (playerSelection.isSelected = false));
  }
}
