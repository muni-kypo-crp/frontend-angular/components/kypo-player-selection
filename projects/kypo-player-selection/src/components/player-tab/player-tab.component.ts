import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { View } from '../../model/enums/view';
import { PlayerSelection } from '../../model/player-selection';

@Component({
  selector: 'kypo-player-tab',
  templateUrl: './player-tab.component.html',
  styleUrls: ['./player-tab.component.scss']
})
export class PlayerTabComponent implements OnInit {

  @Input() playerSelection!: PlayerSelection;
  @Input() view!: View;

  @Output() playerChange: EventEmitter<PlayerSelection> = new EventEmitter();
  @Output() playerHoverChange: EventEmitter<PlayerSelection> = new EventEmitter();
  
  constructor() { }

  ngOnInit(): void {
  }

  togglePlayer() {
    this.playerSelection.isSelected = !this.playerSelection.isSelected; 
    this.playerChange.emit(this.playerSelection)
  }

  onMouseOver(): void {
    this.playerHoverChange.emit(this.playerSelection)
  } 

  onMouseOut(): void {
    this.playerHoverChange.emit(undefined)
  } 

}
