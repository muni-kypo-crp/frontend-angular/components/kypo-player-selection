import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { PlayerSelectionComponent } from './player-selection.component';
import { KypoPlayerSelectionModule } from '@muni-kypo-crp/player-selection';


@NgModule({
  declarations: [PlayerSelectionComponent],
  imports: [
    CommonModule,
    MatCardModule,
    KypoPlayerSelectionModule,
    MatCardModule
  ],
  exports: [PlayerSelectionComponent],
})
export class PlayerSelectionModule {}
